FROM nginx
WORKDIR /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/nginx.conf.template

EXPOSE 80

#ENV ESGFSEARCHENDPOINT https://esg-dn1.nsc.liu.se/esg-search/search

# Set the command to start the node server.
CMD envsubst '${ESGFSEARCHENDPOINT}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf && nginx -g 'daemon off;'



